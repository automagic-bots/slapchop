from pathlib import Path
from botocore.exceptions import ClientError
import boto3
import logging
import os
import urllib.parse


def create_presigned_url(s3_bucket_name, object_name, expiration=86400):
    s3_client = boto3.client('s3')
    try:
        response = s3_client.generate_presigned_url('get_object',
                                                    Params={'Bucket': s3_bucket_name,
                                                            'Key': object_name},
                                                    ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None
    return response


def handler(event, context):
    s3 = boto3.client('s3')

    # Parse Event data
    bucket_name = event['Records'][0]['s3']['bucket']['name']
    # object_metadata = event['headers'].get('x-slapchop-metadata')
    object_name = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')

    # Download file from S3
    print("Starting file download...")
    download_path = f'/tmp/{object_name.split("/")[-1]}'
    s3.download_file(bucket_name, object_name, download_path)
    stripped_file_name = Path(download_path).stem
    sliced_file_name = stripped_file_name + ".gcode"
    print(f"Downloaded {object_name} to {download_path}")

    # Slice file with CuraEngine
    print("Slicing file with CuraEngine...")
    returned_value = os.system(f'/CuraEngine/build/Release/CuraEngine slice -p \
    -j ./profiles/fdm_base/fdmprinter.def.json \
    -j ./profiles/fdm_base/fdmextruder.def.json \
    -j ./profiles/creality/base/creality_base.def.json \
    -j ./profiles/creality/base/creality_base_extruder_0.def.json \
    -j ./profiles/creality/ender3/creality_ender3.def.json \
    -l {download_path} \
    -o /tmp/{sliced_file_name} -s roofing_layer_count=3')
    if returned_value != 0:
        print('File slicing failed.')
        return {
            'statusCode': 500,
            'body': f"Failed to slice {download_path}"
        }
    print(f'{download_path} sliced successfully. Saved to /tmp/{sliced_file_name}')

    # Upload sliced file to S3
    s3.upload_file(f'/tmp/{sliced_file_name}', bucket_name, f'/sliced/{sliced_file_name}')
    url = create_presigned_url(bucket_name, f'/sliced/{sliced_file_name}', 86400)  # URL expires in 24 hours
    if url:
        print("Presigned URL: ", url)
    else:
        print("Failed to generate presigned URL")
        return {
            'statusCode': 500,
            'body': f"Failed to generate download link, file was saved in S3 to /tmp/{sliced_file_name}"
        }

    return {
        'statusCode': 200,
        'body': f"Successfully sliced, download: {url}"
    }

