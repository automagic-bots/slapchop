terraform {
  backend "s3" {
    bucket = "slapchop-terraform"
    key    = "terraform.tfstate"
  }

  required_version = ">= 1.8"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}

data "aws_caller_identity" "current" {}
