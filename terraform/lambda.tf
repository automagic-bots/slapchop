# Lambda function with a Docker container
resource "aws_lambda_function" "this" {
  function_name = local.resource_name
  image_uri     = "${aws_ecr_repository.this.repository_url}:${var.ecr_tag}"
  package_type  = "Image"
  role          = aws_iam_role.lambda_exec_role.arn
  timeout       = 30

  depends_on = [aws_ecr_repository.this]
}

# Trigger lambda on S3 file upload
resource "aws_s3_bucket_notification" "this" {
  bucket = aws_s3_bucket.this.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.this.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "uploads/"
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.this.arn
}
