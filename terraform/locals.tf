locals {
  resource_name = var.custom_name != null ? var.custom_name : "slapchop"
  common_tags = merge({
    project : local.resource_name
    managed : "terraform" },
    var.custom_tags)
}