variable "bucket_name" {
  description = "S3 bucket used to store/retrieve STL/GCode files, settings, and config info."
  type        = string
}

variable "ecr_repo_name" {
  description = "ECR repo used to store the Docker image Lambda uses."
}

variable "ecr_tag" {
  description = "ECR image version to use. (Default: latest)"
  default     = "latest"
}

variable "custom_name" {
  description = "If provided, resource names will use this variable instead of 'slapchop'"
  default     = null
}

variable "custom_tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}