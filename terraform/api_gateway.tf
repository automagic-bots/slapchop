resource "aws_api_gateway_rest_api" "this" {
  name     = "SlapChopAPI"
  tags = local.common_tags
}

resource "aws_api_gateway_resource" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = "upload"
}

resource "aws_api_gateway_method" "put" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this.id
  http_method   = "PUT"
  authorization = "NONE"
  request_parameters = {
    "method.request.header.user" = true
    "method.request.path.object" = true
  }
}

resource "aws_api_gateway_integration" "s3_integration" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = aws_api_gateway_resource.this.id
  http_method = aws_api_gateway_method.put.http_method

  # Direct integration with S3
  type                    = "AWS"
  integration_http_method = "PUT"
  uri                     = "arn:aws:apigateway:us-east-2:s3:path/${aws_s3_bucket.this.bucket}/uploads/{object}"
  credentials             = aws_iam_role.lambda_exec_role.arn

  request_parameters = {
    "integration.request.header.Content-Type" = "'binary/octet-stream'"
    "integration.request.path.object"         = "method.request.path.object"
  }
}

resource "aws_api_gateway_deployment" "this" {
  depends_on = [
    aws_api_gateway_integration.s3_integration,
  ]

  rest_api_id = aws_api_gateway_rest_api.this.id
  stage_name  = "prod"
}