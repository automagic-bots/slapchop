# Example values, replace with your custom values.
BUCKET_NAME=<bucket-name>
FUNCTION_NAME=<lambda-function-name>
IMAGE_NAME=<ecr-image-name>
TEST_STL_NAME=round_cover.stl
TEST_FILE_PATH=./src/sample_stls
REGISTRY_URL=<aws-account-id>.dkr.ecr.<region>.amazonaws.com
VERSION=latest

build:
	docker buildx build --platform linux/amd64 -f ./Dockerfile -t $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION) .

push:
	docker push $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)

deploy:
	aws lambda update-function-code --function-name $(FUNCTION_NAME) --image-uri $(REGISTRY_URL)/$(IMAGE_NAME):$(VERSION)

test:
	aws s3 rm s3://$(BUCKET_NAME)/uploads/$(TEST_STL_NAME)
	aws s3 cp $(TEST_FILE_PATH)/$(TEST_STL_NAME) s3://$(BUCKET_NAME)/uploads/$(TEST_STL_NAME)

all: build push deploy

default: build
