FROM python:3.11 as builder
LABEL authors="TunyFPV"

# Adapted from https://github.com/Ultimaker/CuraEngine/wiki/Building-CuraEngine-From-Source
RUN apt update \
    && apt install -y \
    git \
    gcc \
    cmake \
    protobuf-compiler \
    make

RUN pip install ninja==1.11.1.1 conan==1.64

RUN git clone https://github.com/Ultimaker/CuraEngine.git

RUN conan config install https://github.com/ultimaker/conan-config.git \
    && conan profile new default --detect --force

WORKDIR /CuraEngine

RUN  conan install . --build=missing --update

RUN cmake --preset release \
    && cmake --build --preset release


FROM python:3.11-slim as runtime

RUN apt update
RUN apt install -y gcc

# Setup Lambda RIC ENV vars
ENV LANG=en_US.UTF-8
ENV TZ=:/etc/localtime
ENV PATH=/var/lang/bin:/usr/local/bin:/usr/bin/:/bin:/opt/bin:/CuraEngine/build/Release
ENV LD_LIBRARY_PATH=/var/lang/lib:/lib64:/usr/lib64:/var/runtime:/var/runtime/lib:/var/task:/var/task/lib:/opt/lib:/var/task/lib64:/usr/local/lib:/usr/local/lib64
ENV LAMBDA_TASK_ROOT=/var/task

# Copy CuraEngine build to runtime
COPY --from=builder /CuraEngine /CuraEngine
COPY --from=builder /root/.conan/data/arcus/5.3.1/_/_/package/73537ae16d32fe473aabe384aec52690c806b17c/lib/libArcus.so \
        /usr/local/lib/libArcus.so
COPY --from=builder /root/.conan/data/clipper/6.4.2/ultimaker/stable/package/07ad711d58c6bbd7e981d1318e2ae04c0d5a2968/lib/libpolyclipping.so.22 \
        /usr/local/lib/libpolyclipping.so.22

WORKDIR /var/task

# Copy python src directory to runtime
COPY /src ${LAMBDA_TASK_ROOT}

RUN pip install awslambdaric
RUN pip install \
    -r requirements.txt --target ${LAMBDA_TASK_ROOT}

CMD ["python", "-m", "awslambdaric", "app.handler"]
