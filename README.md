# Slapchop

---

Slapchop aims to be a fully automated STL slicer. Based on [CuraEngine](https://github.com/Ultimaker/CuraEngine), a C++ console application for 3D printing GCode generation. This project includes [Terraform](https://developer.hashicorp.com/terraform/intro) for underlying infrastructure.

### ⚠️ **This is a pre-release version and should be considered a WIP.** ⚠️

---
### Table of Contents
- [Usage](#usage)
- [AWS Setup](#aws-setup)
  - [Prerequisites](#prerequisites)
  - [Terraform](#terraform)
- [Known Issues](#known-issues)

### Usage

#### Current State

- When an STL file is added to the `/uploads` directory in the S3 bucket, this triggers:
  - The Lambda function:
    - Download the file
    - Slice the file (with hard-coded Creality Profile settings - this will be dynamic in a future version)
    - Uploads a `.GCode` file with the same name to the S3 bucket's `/sliced` directory
    - Generates a pre-signed link with a 1-day expiration. (Found in Cloudwatch logs)

#### Future State

In the future, this in intended be a template for a functional static S3 front-end and API backend capable of receiving files, choosing settings from a UI or JSON header data, and will receive a download link


---

## AWS Setup
### Prerequisites
- New or existing AWS Account
- Terraform `v1.8`
- Docker `26.0.1`

### Terraform
Getting started with AWS & Terraform:
https://developer.hashicorp.com/terraform/tutorials/aws-get-started
- Add a custom tfvars file (`/terraform/custom.tfvars`) with unique naming values.
  - Example:
    ``` 
      bucket_name = "automagic-slapchop-slicer"
      ecr_repo_name = "slapchop"
      ```
- **(On first apply only)** Target apply ECR Repo only, upload docker image to ECR repo, then continue. (command in section below)
- Full terraform plan/apply


### Known Issues
When creating the Terraform, the ECR repository will need to be created first in order to upload the Docker image that the lambda will use.
This can be done by running:
`terraform apply -target=aws_ecr_repository.slapchop`